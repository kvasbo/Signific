# Signific

Signific is a super-simple signage app for iOs. It will allow you to show any URL as a full screen view without any user interface elements, and that's pretty much it.

## How to use

Install Signific from the App Store, go to iOs settings -> Signific, and add any valid URL. That's it, you are now done. 