//
//  ViewController.swift
//  Sign
//
//  Created by Audun Kvasbø on 10.02.2018.
//  Copyright © 2018 Rettvendt AS. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKNavigationDelegate {
    
    let urlKeyConstant = "url_preference"
    var hasTried = false
    @IBOutlet weak var myWebView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Attach reaction to settings changes
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.loadPageFromSettings), name: UserDefaults.didChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.loadPageFromSettings), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        myWebView.navigationDelegate = self
        
      //  let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
      // edgePan.edges = .left
        
       // view.addGestureRecognizer(edgePan)
        
        myWebView.allowsBackForwardNavigationGestures = false
        loadPageFromSettings()
    }
    
    // If no page has been set, load a default page.
    @objc func loadPageFromSettings() {
        var urlString = ""
        let defaults = UserDefaults.standard
        if let textFieldValue = defaults.string(forKey: urlKeyConstant) {
            urlString = textFieldValue
            
        }
        if (urlString == "") {
            loadBasePage();
        } else {
            loadUrl(url: urlString)
        }
    }
    
    // Load the built in page.
    func loadBasePage() {
        do {
            guard let filePath = Bundle.main.path(forResource: "indexPage/index", ofType: "html")
        else {
            // File Error
            print ("File reading error")
            return
        }
        let contents =  try String(contentsOfFile: filePath, encoding: .utf8)
        let baseUrl = URL(fileURLWithPath: filePath)
        myWebView.loadHTMLString(contents as String, baseURL: baseUrl)
        }
        catch {
            print ("File HTML error")
        }
    }
    
    // Load the built in error page.
    func loadErrorPage() {
        
        do {
            guard let filePath = Bundle.main.path(forResource: "indexPage/error", ofType: "html")
                else {
                    // File Error
                    print ("File reading error")
                    return
            }
            let contents =  try String(contentsOfFile: filePath, encoding: .utf8)
            let baseUrl = URL(fileURLWithPath: filePath)
            myWebView.loadHTMLString(contents as String, baseURL: baseUrl)
        }
        catch {
            print ("File HTML error")
        }
    }
    
    // Load an url into the web view.
    func loadUrl(url: String) {
        if let checkedUrl = URL(string: url) {
            if(!hasTried) {
                myWebView.load(URLRequest(url: checkedUrl)) // you can use checkedUrl here
                hasTried = true;
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        // dismiss indicator
        
        // if url is not valid {
        //    decisionHandler(.cancel)
        // }
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        // show indicator
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        // dismiss indicator
        // goBackButton.isEnabled = webView.canGoBack
        // goForwardButton.isEnabled = webView.canGoForward
        // navigationItem.title = webView.title
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        // show error dialog
        // print("Load error", error.code, error.domain)
        // https://gist.github.com/samirahmed/ee52a076dd19966c4c9d
        
        if(error.code != -999) {
            DispatchQueue.main.async { // Correct
                self.loadErrorPage()
            }
        }
    }
    
}

extension Error {
    var code: Int { return (self as NSError).code }
    var domain: String { return (self as NSError).domain }
}
